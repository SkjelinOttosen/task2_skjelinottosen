﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task2_SkjelinOttosen
{
    class Print
    {
        public static void PrintSquare(int length, int height )
        {
            Console.WriteLine(" The square is " + length + " x "+ height);
            // Set the Foreground color to dark yellow
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    if(i == 0 || i == height-1 ||  j == 0 || j == length-1) {
                         Console.Write(" # ");  
                    }
                    else 
                    {
                        Console.Write("   ");
                    }               
                }

                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}
