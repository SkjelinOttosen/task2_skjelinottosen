﻿using System;
using System.Linq.Expressions;

namespace Task2_SkjelinOttosen
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true) {
                try
                {
                    Console.WriteLine("Enter the side length of a square:  ");
                    int length = Int16.Parse(Console.ReadLine());
                    if (length < 1)
                    {
                        throw new ArgumentOutOfRangeException();
                    }

                    Console.WriteLine("Enter the side height of a square: ");
                    int height = Int16.Parse(Console.ReadLine());

                    if (height < 1)
                    {
                        throw new ArgumentOutOfRangeException();
                    }
                    Console.WriteLine();

                    Print.PrintSquare(length, height);
                    // Set the Foreground color to white
                    Console.ForegroundColor = ConsoleColor.White;
                  
                }
                catch (ArgumentOutOfRangeException ex)
                {  
                    // Set the Foreground color to dark red
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine(ex.Message);
                    // Set the Foreground color to white
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine();

                }
                catch(Exception ex) 
                {
                    // Set the Foreground color to dark red
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine(ex.Message);
                    // Set the Foreground color to white
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine();
                }
            }        
        }
    }
}
